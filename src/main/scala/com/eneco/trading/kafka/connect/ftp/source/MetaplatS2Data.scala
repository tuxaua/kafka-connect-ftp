package com.eneco.trading.kafka.connect.ftp.source

import org.apache.kafka.connect.data.{ Schema, SchemaBuilder, Struct }

case class MetaplatS2Data(
  keggOrthologue:              String,
  microbialGene:               String,
  relativeAbundance2019N00001: Double,
  relativeAbundance2019N00002: Double,
  relativeAbundance2019N00003: Double,
  relativeAbundance2019N00004: Double,
  relativeAbundance2019N00005: Double,
  relativeAbundance2019N00006: Double,
  relativeAbundance2019N00007: Double,
  relativeAbundance2019N00008: Double) {

  val schema = SchemaBuilder.struct()
    .doc("Metagenomic data for the analysis of the associations between relative abundances of microbial genes with meth ane emissions and feed conversion efficiency")
    .name("de.feu.lgmmia.crisp4bigdata.converter.MetaplatS2Data")
    .field("keggOrthologue", Schema.STRING_SCHEMA)
    .field("microbialGene", Schema.STRING_SCHEMA)
    .field("relativeAbundance2019N00001", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00002", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00003", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00004", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00005", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00006", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00007", Schema.FLOAT64_SCHEMA)
    .field("relativeAbundance2019N00008", Schema.FLOAT64_SCHEMA)
    .build()

  def content = new Struct(schema)
    .put("keggOrthologue", keggOrthologue)
    .put("microbialGene", microbialGene)
    .put("relativeAbundance2019N00001", relativeAbundance2019N00001)
    .put("relativeAbundance2019N00002", relativeAbundance2019N00002)
    .put("relativeAbundance2019N00003", relativeAbundance2019N00003)
    .put("relativeAbundance2019N00004", relativeAbundance2019N00004)
    .put("relativeAbundance2019N00005", relativeAbundance2019N00005)
    .put("relativeAbundance2019N00006", relativeAbundance2019N00006)
    .put("relativeAbundance2019N00007", relativeAbundance2019N00007)
    .put("relativeAbundance2019N00008", relativeAbundance2019N00008)

}
