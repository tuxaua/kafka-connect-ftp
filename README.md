# Kafka Connect FTP

Source connector for Kafka Connect to monitor directories on an FTP server and inject files as records into Kafka

## Configuration

The following specific configuration parameters are supported in addition to the general parameters that apply to all connectors.

| Name                        | Type      | Required | Description                                      |
|:----------------------------|:----------|:---------|:-------------------------------------------------|
| `ftp.address`               | String    | yes      | host\[:port\] of the ftp server                  |
| `ftp.user`                  | String    | yes      | user name                                        |
| `ftp.password`              | String    | yes      | password                                         |
| `ftp.refresh`               | String    | yes      | polling interval as iso8601 duration             |
| `ftp.file.maxage`           | String    | yes      | upper limit for age of files as iso8601 duration |
| `ftp.keystyle`              | String    | yes      | `string` or `struct`                             |
| `ftp.monitor.tail`          | List      | no       | comma separated list of src-path:dest-topic      |
| `ftp.monitor.update`        | List      | no       | comma separated list of src-path:dest-topic      |
| `ftp.sourcerecordconverter` | String    | no       | Source Record converter class name               |

See example from the Cognitive Walkthrough [here](./example.properties).

## Tail Versus Update

*Tailed* files are *only* allowed to grow. Bytes that have been appended to it since a last inspection are processed. Any changes in the leading part are ignored.

*Updated* files can grow, shrink, and change anywhere. The whole content is processed.

## Source Record Converter

`MetaplatS2Converter` converts sample data from the Metaplat project into Kafka Connect records.
